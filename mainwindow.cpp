#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QResizeEvent>
#include <QScrollBar>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    messageCount = 0;
    twosCount = 0;

    ctr = 0;

    ui->statusBar->showMessage("ISOBUS Board Not Found");

     usb = new CUSBDevice(0x4d8,0x04b0,0x01);     // Monitor & Flash Control
   //usb = new CUSBDevice(0x4d8,0x04b1,0x01);    // Bridge (only way to monitor when bridge is in loopback mode)
     connect(usb,SIGNAL(ISO(dataPack)),this,SLOT(ISO(dataPack)));
     connect(usb,SIGNAL(dataReady(int,int)), this, SLOT(DataReady(int,int)));

     connect(usb,SIGNAL(attached()),this,SLOT(usb_attached()));
     connect(usb,SIGNAL(removed()),this, SLOT(usb_removed()));
     usb->checkAttached();

     usb->start();

     usb2 = new CUSBDevice(0x4d8,0x04b1,0x01);
     usb2->checkAttached();
     usb2->start();


ui->tableWidget->setColumnWidth(0,50);
ui->tableWidget->setColumnWidth(1,50);
ui->tableWidget->setColumnWidth(2,50);
ui->tableWidget->setColumnWidth(3,50);
ui->tableWidget->setColumnWidth(4,50);
ui->tableWidget->setColumnWidth(5,50);

ui->tableWidget->setColumnWidth(6,50);
ui->tableWidget->setColumnWidth(7,50);
ui->tableWidget->setColumnWidth(8,50);
ui->tableWidget->setColumnWidth(9,80);
ui->tableWidget->setColumnWidth(10,50);


ui->tableWidget->setColumnWidth(11,50);
ui->tableWidget->setColumnWidth(12,50);
ui->tableWidget->setColumnWidth(13,50);
ui->tableWidget->setColumnWidth(14,50);
ui->tableWidget->setColumnWidth(15,50);
ui->tableWidget->setColumnWidth(16,50);
ui->tableWidget->setColumnWidth(17,50);
ui->tableWidget->setColumnWidth(18,50);

ui->tableWidget->setColumnWidth(19,220);

//for(int i=0;i<ui->filterTable->rowCount();i++)
//{
//ui->filterTable->item(i,2)->setCheckState(Qt::Checked);
//ui->filterTable->item(i,2)->setText("");
//}
ui->progressBar->setValue(0);

    connect(&scrollTimer,SIGNAL(timeout()),this,SLOT(scrollTimeout()));
        scrollTimer.start(1);

    connect(ui->tableWidget->verticalScrollBar(),SIGNAL(sliderPressed()),this,SLOT(scrollBarPressed()));
    connect(ui->tableWidget->verticalScrollBar(),SIGNAL(sliderReleased()),this,SLOT(scrollBarReleased()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
    int w = e->size().width();
    int h = e->size().height();

    ui->tabWidget->setGeometry(ui->tabWidget->x(),ui->tabWidget->y(),w,h-115);
    ui->tableWidget->setGeometry(ui->tableWidget->x(),ui->tableWidget->y(),w-7,h-145);

    ui->tableWidget->setColumnWidth(19,ui->tableWidget->width()-982);
}

bool MainWindow::nativeEvent(const QByteArray &eventType, void *mess, long *result)
{
    MSG *m;
    m = (MSG*)mess;
    if(m->message == WM_DEVICECHANGE)
    {

        if(m->wParam == DBT_DEVNODES_CHANGED)
        {
            usb->checkAttached();
            usb2->checkAttached();
        }
    }
    return false;
}

void MainWindow::usb_attached()
{
    ui->statusBar ->showMessage("ISOBUS Board Attached");
}

void MainWindow::usb_removed()
{
   ui->statusBar ->showMessage("ISOBUS Board Removed");
}

void MainWindow::on_pushButton_clicked()
{
usb->writeCommand(0x01, 0x00);
}

void MainWindow::on_pushButton_2_clicked()
{
  usb->writeCommand(0x02, 0x00);
}

void MainWindow::decodeLightCommand(framex f)
{
    for(int i=0;i<ui->lightsTable->rowCount();i++)
    {
        ui->lightsTable->item(i,0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    }


    setItemState(0, (f.data[0] >> 6) & 0x03);
    setItemState(1, (f.data[0] >> 4) & 0x03);
    setItemState(2, (f.data[0] >> 2) & 0x03);
    setItemState(3,  f.data[0] & 0x03);


    setItemState(4, (f.data[1] >> 6) & 0x03);
    setItemState(5, (f.data[1] >> 4) & 0x03);
    setItemState(6, (f.data[1] >> 2) & 0x03);
    setItemState(7,  f.data[1] & 0x03);


    setItemState(8, (f.data[2] >> 6) & 0x03);
    setItemState(9, (f.data[2] >> 4) & 0x03);
    setItemState(10,(f.data[2] >> 2) & 0x03);
    setItemState(11, f.data[2] & 0x03);

    setItemState(12, (f.data[3] >> 6) & 0x03);
    setItemState(13, (f.data[3] >> 4) & 0x03);
    setItemState(14, (f.data[3] >> 2) & 0x03);
    setItemState(15,  f.data[3] & 0x03);

    setItemState(16, (f.data[4] >> 6) & 0x03);
    setItemState(17, (f.data[4] >> 4) & 0x03);
    setItemState(18, (f.data[4] >> 2) & 0x03);
    setItemState(19,  f.data[4] & 0x03);

    setItemState(20, (f.data[5] >> 6) & 0x03);
    setItemState(21, (f.data[5] >> 4) & 0x03);
    setItemState(22, (f.data[5] >> 2) & 0x03);
    setItemState(23,  f.data[5] & 0x03);

    setItemState(24, (f.data[6] >> 6) & 0x03);
    setItemState(25, (f.data[6] >> 4) & 0x03);
    setItemState(26, (f.data[6] >> 2) & 0x03);
    setItemState(27,  f.data[6] & 0x03);

    setItemState(28, (f.data[7] >> 6) & 0x03);
    setItemState(29, (f.data[7] >> 4) & 0x03);
    setItemState(30, (f.data[7] >> 2) & 0x03);
    //setItemState(31,  f.data[7] & 0x03);
}

void MainWindow::setItemState(int index, int state)
{
    if(state == 0)
    {
        ui->lightsTable->item(index,0)->setBackgroundColor(QColor(255,50,0));
        ui->lightsTable->item(index,0)->setText("OFF");
    }
    if(state == 1)
    {
        ui->lightsTable->item(index,0)->setBackgroundColor(QColor(0,255,50));
        ui->lightsTable->item(index,0)->setText("ON");
    }
    if(state == 3)
    {
        ui->lightsTable->item(index,0)->setBackgroundColor(QColor(190,190,190));
        ui->lightsTable->item(index,0)->setText("---");
    }
}

void MainWindow::decodePTO(framex f, QTableWidget *wid)
{
    for(int i=0;i<wid->rowCount();i++)wid->item(i,0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    int rpm;
    rpm = (int)(f.data[0] | (f.data[1] << 8) );
    if(rpm <= 64255)    // Valid
    {
        rpm *= 0.125;
        wid->item(0,0)->setText(QString("%1rpm").arg(rpm));
    }
    else
    {
        if(rpm >= 64256 && rpm < 64512) wid->item(0,0)->setText("-psi-");
        if(rpm >= 65024 && rpm < 65280) wid->item(0,0)->setText("ERROR");
        if(rpm >= 65280) wid->item(0,0)->setText("---");
    }

    rpm = (int)(f.data[2] | (f.data[3] << 8) );

    if(rpm <= 64255)    // Valid
    {
        rpm *= 0.125;
        wid->item(1,0)->setText(QString("%1rpm").arg(rpm));
    }
    else
    {
        if(rpm >= 64256 && rpm < 64512) wid->item(1,0)->setText("-psi-");
        if(rpm >= 65024 && rpm < 65280) wid->item(1,0)->setText("ERROR");
        if(rpm >= 65280) wid->item(1,0)->setText("---");
    }

    int state = (f.data[4] >> 6) & 0x03;
    if( state == 0 )
    {
       wid->item(2,0)->setText("OFF");
       wid->item(2,0)->setBackgroundColor(QColor(255,50,0));
    }
    if( state == 1 )
    {
       wid->item(2,0)->setText("ON");
       wid->item(2,0)->setBackgroundColor(QColor(0,255,50));
    }
    if( state == 2 )
    {
       wid->item(2,0)->setText("ERROR");
       wid->item(2,0)->setBackgroundColor(QColor(255,0,0));
    }
    if( state == 3 )
    {
       wid->item(2,0)->setText("---");
       wid->item(2,0)->setBackgroundColor(QColor(190,190,190));
    }


    state = (f.data[4] >> 4) & 0x03;
    if(state == 0)  wid->item(3,0)->setText("540");
    if(state == 1)  wid->item(3,0)->setText("1000");
    if(state == 2)  wid->item(3,0)->setText("ERROR");
    if(state == 3)  wid->item(3,0)->setText("---");

    state = (f.data[4] >> 2) & 0x03;
    if(state == 1)  wid->item(3,0)->setText( wid->item(3,0)->text() + QString("E"));

    state = (f.data[4] ) & 0x03;
    if(state == 0)  wid->item(4,0)->setText("OK");
    if(state == 1)  wid->item(4,0)->setText("Override");
    if(state == 2)  wid->item(4,0)->setText("Error");
    if(state == 3)  wid->item(4,0)->setText("---");

    state = (f.data[5] >> 6) & 0x03;
    if(state == 0)  wid->item(5,0)->setText("OK");
    if(state == 1)  wid->item(5,0)->setText("Override");
    if(state == 2)  wid->item(5,0)->setText("Error");
    if(state == 3)  wid->item(5,0)->setText("---");

    state = (f.data[5] >> 4) & 0x03;
    if(state == 0)  wid->item(6,0)->setText("OK");
    if(state == 1)  wid->item(6,0)->setText("Override");
    if(state == 2)  wid->item(6,0)->setText("Error");
    if(state == 3)  wid->item(6,0)->setText("---");

    state = (f.data[5] >> 1) & 0x07;
    if(state == 0)  wid->item(7,0)->setText("Not Limited");
    if(state == 1)  wid->item(7,0)->setText("Operater Limited");
    if(state == 2)  wid->item(7,0)->setText("Limited High");
    if(state == 3)  wid->item(7,0)->setText("Limited Low");
    if(state == 4)  wid->item(7,0)->setText("?");
    if(state == 5)  wid->item(7,0)->setText("?");
    if(state == 6)  wid->item(7,0)->setText("Fault");
    if(state == 7)  wid->item(7,0)->setText("---");
}

void MainWindow::decodeSCV(framex f, QTableWidget *wid)
{
    for(int i=0;i<wid->rowCount();i++)wid->item(i,0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    if(f.data[0] <251)
    {
        int v = -125 + f.data[0];
        wid->item(0,0)->setText(QString("%1%").arg(v));
    }
    if(f.data[0] == 251) wid->item(0,0)->setText("-psi-");
    if(f.data[0] == 254) wid->item(0,0)->setText("Error");
    if(f.data[0] == 255) wid->item(0,0)->setText("---");



    if(f.data[1] <251)
    {
        int v = -125 + f.data[1];
        wid->item(1,0)->setText(QString("%1%").arg(v));
    }
    if(f.data[1] == 251) wid->item(1,0)->setText("-psi-");
    if(f.data[1] == 254) wid->item(1,0)->setText("Error");
    if(f.data[1] == 255) wid->item(1,0)->setText("---");

    int state;
    state = (f.data[2] >> 6) & 0x03;
    if(state == 0)  wid->item(2,0)->setText("Block");
    if(state == 1)  wid->item(2,0)->setText("Float");
    if(state == 2)  wid->item(2,0)->setText("Error");
    if(state == 3)  wid->item(2,0)->setText("---");

    state = (f.data[2] ) & 0x0F;
    if(state == 0)  wid->item(3,0)->setText("Block");
    if(state == 1)  wid->item(3,0)->setText("Extend");
    if(state == 2)  wid->item(3,0)->setText("Retract");
    if(state == 3)  wid->item(3,0)->setText("Floating");

    if(state == 14)  wid->item(3,0)->setText("Error");
    if(state == 15)  wid->item(3,0)->setText("---");

     state = (f.data[4] ) & 0b00111111;
     if(state == 0)  wid->item(4,0)->setText("Ok");
     if(state == 1)  wid->item(4,0)->setText("Operator Not Present");
     if(state == 2)  wid->item(4,0)->setText("Imp Released Ctrl");
     if(state == 3)  wid->item(4,0)->setText("Operator Overide");
     if(state == 4)  wid->item(4,0)->setText("Operator Ctl Not Valid Pos");
     if(state == 5)  wid->item(4,0)->setText("Remote Cmd Timeout");
     if(state == 6)  wid->item(4,0)->setText("Remote Cmd Invalid");
     if(state == 7)  wid->item(4,0)->setText("Func Not Calibrated");
     if(state == 8)  wid->item(4,0)->setText("Operator Ctl Fault");
     if(state == 9)  wid->item(4,0)->setText("Function Fault");
     if(state == 0b10100)  wid->item(4,0)->setText("Oil Low");
     if(state == 0b10101)  wid->item(4,0)->setText("Valve Locked Out");
     if(state == 0b111110)  wid->item(4,0)->setText("Error");
     if(state == 0b111111)  wid->item(4,0)->setText("---");
}

void MainWindow::decodeSCVmeasured(framex f, QTableWidget *wid)
{
    if(f.data[0] <251)
    {
        int v = -125 + f.data[0];
        wid->item(0,0)->setText(QString("%1%").arg(v));
    }
    if(f.data[0] == 251) wid->item(0,0)->setText("-psi-");
    if(f.data[0] == 254) wid->item(0,0)->setText("Error");
    if(f.data[0] == 255) wid->item(0,0)->setText("---");



    if(f.data[1] <251)
    {
        int v = -125 + f.data[1];
        wid->item(1,0)->setText(QString("%1%").arg(v));
    }
    if(f.data[1] == 251) wid->item(1,0)->setText("-psi-");
    if(f.data[1] == 254) wid->item(1,0)->setText("Error");
    if(f.data[1] == 255) wid->item(1,0)->setText("---");

    int rpm;
    rpm = (int)(f.data[2] | (f.data[3] << 8) );
    if(rpm <= 64255)    // Valid
    {
        rpm *= 0.145038;
        wid->item(2,0)->setText(QString("%1psi").arg(rpm));
    }
    else
    {
        if(rpm >= 64256 && rpm < 64512) wid->item(2,0)->setText("-psi-");
        if(rpm >= 65024 && rpm < 65280) wid->item(2,0)->setText("ERROR");
        if(rpm >= 65280) wid->item(2,0)->setText("---");
    }

    rpm = (int)(f.data[4] | (f.data[5] << 8) );
    if(rpm <= 64255)    // Valid
    {
        rpm *= 0.145038;
        wid->item(3,0)->setText(QString("%1psi").arg(rpm));
    }
    else
    {
        if(rpm >= 64256 && rpm < 64512) wid->item(3,0)->setText("-psi-");
        if(rpm >= 65024 && rpm < 65280) wid->item(3,0)->setText("ERROR");
        if(rpm >= 65280) wid->item(3,0)->setText("---");
    }

    rpm = (int)(f.data[6] | (f.data[7] << 8) );
    if(rpm <= 64255)    // Valid
    {
        rpm *= 0.145038;
        wid->item(4,0)->setText(QString("%1psi").arg(rpm));
    }
    else
    {
        if(rpm >= 64256 && rpm < 64512) wid->item(4,0)->setText("-psi-");
        if(rpm >= 65024 && rpm < 65280) wid->item(4,0)->setText("ERROR");
        if(rpm >= 65280) wid->item(4,0)->setText("---");
    }

}

void MainWindow::refilter()
{
    QVector<framex> t = frames;
    frames.clear();
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);

    for(QVector<framex>::iterator i=t.begin() ; i!=t.end();i++)
    {
        dataPack d;
        d.count = 1;
        d.nMessages = 0;
        d.frames[0] = *i;
        ISO(d);
    }
}

void MainWindow::ISO(dataPack d)
{
    if(d.count > 0)
    {
        messageCount += d.count;
        //setWindowTitle(QString("%1 - %2 - %3 - (%4)").arg(d.nBytes).arg(d.nMessages).arg(d.count).arg(messageCount));
        for(int i=0;i<d.count;i++)
        {

            // Light Command Decode
            if(d.frames[i].pgn == 65089) decodeLightCommand(d.frames[i]);
            // Rear PTO Decode
            if(d.frames[i].pgn == 65091) decodePTO(d.frames[i], ui->rearPTO);
            // Front PTO Decode
            if(d.frames[i].pgn == 65092) decodePTO(d.frames[i], ui->frontPTO);

            if(d.frames[i].pgn == 65041) decodeSCV(d.frames[i], ui->scv1);
            if(d.frames[i].pgn == 65042) decodeSCV(d.frames[i], ui->scv2);
            if(d.frames[i].pgn == 65043) decodeSCV(d.frames[i], ui->scv3);
            if(d.frames[i].pgn == 65044) decodeSCV(d.frames[i], ui->scv4);
            if(d.frames[i].pgn == 65054) decodeSCV(d.frames[i], ui->scv14);

           // if(d.frames[i].pgn == 65057) decodeSCVmeasured(d.frames[i], ui->scvMeasured);
           // if(d.frames[i].pgn == 65058) decodeSCVmeasured(d.frames[i], ui->scvMeasured_2);
           // if(d.frames[i].pgn == 65059) decodeSCVmeasured(d.frames[i], ui->scvMeasured_3);
           // if(d.frames[i].pgn == 65060) decodeSCVmeasured(d.frames[i], ui->scvMeasured_4);
           // if(d.frames[i].pgn == 65070) decodeSCVmeasured(d.frames[i], ui->scvMeasured_5);


            frames.append(d.frames[i]);
           // ui->textBrowser->append(QString("%1 - %2 - %3 - %4").arg(d.id[i]).arg(d.bytes[i][0]).arg(d.bytes[i][1]).arg(d.bytes[i][2]));

            if(ui->cbFilter->isChecked())
            {
                bool addRow = false;

                if(ui->cb_filterAddr->isChecked())
                {
                for(int ii=0;ii<ui->filterTable->rowCount();ii++)
                {
                    QTableWidgetItem *n = ui->filterTable->item(ii,2);
                    if(n->checkState() == Qt::Checked)
                    {
                        if(d.frames[i].sa == ui->filterTable->item(ii,0)->text().toInt() && d.frames[i].ps == ui->filterTable->item(ii,1)->text().toInt()) addRow = true;
                    }
                }
                }

                if(ui->cbPgnFilter->isChecked())
                {
                    bool filter = false;
                    for(int ii=0;ii<ui->pgnFilters->rowCount();ii++)
                    {
                        QTableWidgetItem *n = ui->pgnFilters->item(ii,1);
                        if(n->checkState() == Qt::Checked)
                        {
                            if(d.frames[i].pgn == ui->pgnFilters->item(ii,0)->text().toInt()) filter = true;
                        }
                    }
                    if(addRow && !filter) addRow = false;
                    if(!ui->cb_filterAddr->isChecked() && filter) addRow = true;
                }

                if(ui->cbAddressClaims->isChecked())
                {

                    if(d.frames[i].pgn == 60928) addRow = true;
                    if(d.frames[i].pgn == 59904)
                    {
                        int p = d.frames[i].data[0] | (d.frames[i].data[1] << 8) | (d.frames[i].data[2] << 16);

                        if(p == 60928) addRow = true;
                    }
                }

                if(!addRow) continue; //return;
            }

            ui->tableWidget->setRowCount(ui->tableWidget->rowCount()+1);
            QTableWidgetItem *ii = new QTableWidgetItem();



            // Messages in RB
            ii->setText(QString("%1").arg(d.nMessages));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,0,ii);
            // Messages in USB
            ii = new QTableWidgetItem(QString("%1").arg(d.count));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,1,ii);

            // PRIORITY
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].priority));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,2,ii);
            // EDP
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].edp));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,3,ii);
            // DP
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].dp));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,4,ii);
            // EXID
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].ide));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,5,ii);
            // PF
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].pf));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,6,ii);
            // PS
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].ps));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,7,ii);
            // SA
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].sa));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,8,ii);
            // PGN
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].pgn));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,9,ii);
            // DLC
            ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].dlc));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,10,ii);
            // DATA
            for(int n=0;n<8;n++)
            {
                ii = new QTableWidgetItem(QString("%1").arg(d.frames[i].data[n]));
                ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,11+n,ii);

                if( d.frames[i].dlc < n+1)
                {
                    ii->setBackgroundColor(QColor(Qt::lightGray));
                    ii->setTextColor(QColor(Qt::gray));
                }
            }


            //********************************************************************
                framex f = d.frames[i];
                    // QTableWidgetItem *ii = new QTableWidgetItem();
                    ii = new QTableWidgetItem(QString(""));
                    ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,19,ii);

                    if(f.pgn == 51968)
                    {   // Task Controller
                        ii->setText("TC: ");
                        quint8 cmd = f.data[0] & 0x0F;
                        if(cmd == 0)    // Tech Capabilities
                        {
                            quint8 sub = (f.data[0]  & 0xF0) >> 4;

                            if(sub == 0)
                            {   // Request Version
                                ii->setText(ii->text() + "Request Version");
                                ii->setBackground(QColor(Qt::green));
                            }
                            if(sub == 1)
                            {   // Version Message
                                ii->setText(ii->text() + QString("Version = %1").arg((quint8)f.data[1]));
                                ii->setBackground(QColor(Qt::green));
                            }
                        }
                        if(cmd == 1)    // Device Desciptor Managment
                        {
                            quint8 sub = (f.data[0]  & 0xF0) >> 4;

                            if(sub == 0)    // Request Structure Label
                            {
                                ii->setText(ii->text() + "Request Structure Label");
                                ii->setBackground(QColor(Qt::green));
                            }
                            if(sub == 1)    // Structure Label
                            {
                                ii->setText(ii->text() + "Structure Label");
                                ii->setBackground(QColor(Qt::green));
                            }
                            if(sub == 2)    // Request Localization Label
                            {
                                ii->setText(ii->text() + "Request Localization Label");
                                ii->setBackground(QColor(Qt::green));
                            }
                            if(sub == 3)    // Localization Label
                            {
                                ii->setText(ii->text() + "Localization Label");
                                ii->setBackground(QColor(Qt::green));
                            }
                            if(sub == 4)    // Request Pool Xfer
                            {
                                quint32 nBytes;
                                nBytes = f.data[1];
                                nBytes |= (f.data[2] << 8);
                                nBytes |= (f.data[3] << 16);
                                nBytes |= (f.data[4] << 24);
                                ii->setText(ii->text() + QString("Request Object-pool Transfer [%1 bytes]").arg(nBytes));
                                ii->setBackground(QColor(Qt::green));
                            }
                            if(sub == 5)    // Request Pool Xfer response
                            {
                                ii->setText(ii->text() + QString("Request Object-pool Transfer Response: "));
                                if(f.data[1] == 0) ii->setText(ii->text() + "OK");
                                if(f.data[1] == 1) ii->setText(ii->text() + "Not Enough Memory");
                                ii->setBackground(QColor(Qt::green));
                            }

                            if(sub == 6)    // Object Pool Transfer
                            {

                            }

                            if(sub == 7) // Object-pool transfer response
                            {
                                QString out("Object-pool Transfer Response: ");

                                quint32 nBytes;
                                nBytes = f.data[2];
                                nBytes |= (f.data[3] << 8);
                                nBytes |= (f.data[4] << 16);
                                nBytes |= (f.data[5] << 24);

                                out += QString("%1 bytes | ").arg(nBytes);

                                if(f.data[1] == 0) out += "OK";
                                else out+= "Error";

                                 ii->setText(ii->text() + out);
                                 ii->setBackground(QColor(Qt::green));
                            }
                            if(sub == 8)
                            {
                                if(f.data[1] == 0xFF)ii->setText(ii->text() + "Object-pool Activate");
                                else ii->setText(ii->text() + "Object-pool Deactivate");
                                ii->setBackground(QColor(Qt::green));
                            }
                            if(sub == 9)
                            {
                                ii->setText(ii->text() + "Object-pool Activate Response: ");
                                if(f.data[1] == 0) ii->setText(ii->text() + "No Errors");
                                else ii->setText(ii->text() + "ERRORS");
                            }
                        }
                        if(cmd == 2)    // Request Value
                        {
                            QString out("Request Value: ");

                            quint16 element;
                            quint16 ddi;

                            element = (f.data[0]  & 0xF0) >> 4;
                            element |= (f.data[1] << 4);

                            ddi = f.data[2];
                            ddi |= (f.data[3] << 8);

                            out += QString("Element=%1 DDI=%2").arg(element).arg(ddi);

                            ii->setText(ii->text() + out);
                            ii->setBackground(QColor(Qt::yellow));
                        }

                        if(cmd == 3)    // Value
                        {
                            QString out("Value: ");

                            quint16 element;
                            quint16 ddi;

                            element = (f.data[0]  & 0xF0) >> 4;
                            element |= (f.data[1] << 4);

                            ddi = f.data[2];
                            ddi |= (f.data[3] << 8);

                            qint32 value = f.data[4];
                            value |= (f.data[5] << 8);
                            value |= (f.data[6] << 16);
                            value |= (f.data[7] << 24);

                            out += QString("Element=%1 DDI=%2 Value=%3").arg(element).arg(ddi).arg(value);

                            ii->setText(ii->text() + out);
                            ii->setBackground(QColor(Qt::darkYellow));
                        }

                        if(cmd == 4)    // Measurement Time Interval
                        {
                            QString out("Time Interval: ");

                            quint16 element;
                            quint16 ddi;

                            element = (f.data[0]  & 0xF0) >> 4;
                            element |= (f.data[1] << 4);

                            ddi = f.data[2];
                            ddi |= (f.data[3] << 8);

                            qint32 value = f.data[4];
                            value |= (f.data[5] << 8);
                            value |= (f.data[6] << 16);
                            value |= (f.data[7] << 24);

                            out += QString("Element=%1 DDI=%2 Value=%3ms").arg(element).arg(ddi).arg(value);

                            ii->setText(ii->text() + out);
                            ii->setBackground(QColor(Qt::cyan));
                        }

                        if(cmd == 8)    // Measurement Change Threshold
                        {
                            QString out("Change Threshold: ");

                            quint16 element;
                            quint16 ddi;

                            element = (f.data[0]  & 0xF0) >> 4;
                            element |= (f.data[1] << 4);

                            ddi = f.data[2];
                            ddi |= (f.data[3] << 8);

                            qint32 value = f.data[4];
                            value |= (f.data[5] << 8);
                            value |= (f.data[6] << 16);
                            value |= (f.data[7] << 24);

                            out += QString("Element=%1 DDI=%2 Value=%3").arg(element).arg(ddi).arg(value);

                            ii->setText(ii->text() + out);
                            ii->setBackground(QColor(255,127,0));
                        }

                        if(cmd == 13)   // PDACK
                        {
                          ii->setText(ii->text() + "Process Data Acknowledge (PDACK)");
                        }

                        if(cmd == 14) // TC Status
                        {
                            if( f.data[4] & 1 )
                            ii->setText(ii->text() + "Status [Task Totals Active: Yes]");
                            else
                            ii->setText(ii->text() + "Status [Task Totals Active: No]");
                            ii->setBackground(QColor(Qt::magenta));
                        }
                        if( cmd == 15) // Client Task Message
                        {
                           ii->setText(ii->text() + "Client Task Message");
                           ii->setBackground(QColor(Qt::magenta));
                        }
                    }

                    if((f.pgn == 58880)  )
                    {
                        ii->setText("VT to ECU");
                        if( f.data[0] == 0) ii->setText(ii->text() + " Soft Key Activation");
                        if( f.data[0] == 1) ii->setText(ii->text() + " Button Activation");
                        if( f.data[0] == 2) ii->setText(ii->text() + " Pointing Event");

                        if( f.data[0] == 34)
                        {
                            ii->setText(ii->text() + " - Prefered Assignment Response");
                            ii->setBackgroundColor(QColor(Qt::darkGreen));
                        }

                        if( f.data[0] == 36) ii->setText(ii->text() + " AUX-N Assignment");

                        if( f.data[0] == 37) ii->setText(ii->text() + " AUX-N Status Enable");
                        if( f.data[0] == 38) ii->setText(ii->text() + " AUX-N Status");
                        if( f.data[0] == 35) ii->setText(ii->text() + " AUX-N Maintenance");



                        if( f.data[0] == 192) ii->setText(ii->text() + " - Get Memory Response");
                        if( f.data[0] == 194) ii->setText(ii->text() + " - Get Number of Soft Keys Response");
                        if( f.data[0] == 195) ii->setText(ii->text() + " - Get Text Font Data Response");
                        if( f.data[0] == 199) ii->setText(ii->text() + " - Get Hardware Response");
                        if( f.data[0] == 193) ii->setText(ii->text() + " - Get Supported WChars Response");
                        if( f.data[0] == 197) ii->setText(ii->text() + " - Get Window Mask Data Response");
                        if( f.data[0] == 198) ii->setText(ii->text() + " - Get Supported Objects Response");

                        if( f.data[0] == 223) ii->setText(ii->text() + " - Get Versions Response");
                        if( f.data[0] == 208) ii->setText(ii->text() + " - Store Version Response");
                        if( f.data[0] == 209) ii->setText(ii->text() + " - Load Version Response");
                        if( f.data[0] == 210) ii->setText(ii->text() + " - Delete Version Response");
                        if( f.data[0] == 211) ii->setText(ii->text() + " - Ext Get Versions Response");
                        if( f.data[0] == 212) ii->setText(ii->text() + " - Ext Store Version Response");
                        if( f.data[0] == 213) ii->setText(ii->text() + " - Ext Load Version Response");
                        if( f.data[0] == 214) ii->setText(ii->text() + " - Ext Delete Version Response");

                        if( f.data[0] == 160 ) ii->setText(ii->text() + " - Hide/Show Object Response");
                        if( f.data[0] == 161) ii->setText(ii->text() + " - Enable/Disable Object Response");
                        if( f.data[0] == 162) ii->setText(ii->text() + " - Select Input Object Response");
                        if( f.data[0] == 146) ii->setText(ii->text() + " - ESC Response");

                         if( f.data[0] == 165) ii->setText(ii->text() + " - Change Child Location Response");
                         if( f.data[0] == 180) ii->setText(ii->text() + " - Change Child Position Response");
                         if( f.data[0] == 168)
                         {
                             ii->setText(ii->text() + " - Change Numeric Value Response");
                         }
                         if( f.data[0] == 179)
                         {
                             ii->setText(ii->text() + " - Change String Value Response");
                         }




                        if( f.data[0] == 254)
                        {
                            ii->setText(ii->text() + " - VT Status");
                            ii->setBackgroundColor(QColor(Qt::yellow));
                        }

                        if( f.data[0] == 5)
                        {
                            int val = 0;
                            for(int ic=7;ic>3;ic--)
                            {
                                val = val << 8;
                                val |= f.data[ic];
                            }
                            ii->setText(ii->text() + QString(" - VT Change Numeric Value (%1)").arg(val));
                        }

                        if( f.data[0] == 6)
                        {
                            ii->setText(ii->text() + " - Change Active Mask");
                        }

                        if( f.data[0] == 7)
                        {
                            ii->setText(ii->text() + " - Change Softkey Mask");
                        }
                    }

                    if( (f.pgn == 59136)  )
                    {
                        ii->setText("ECU to VT");

                        if( f.data[0] == 17)
                        {
                            ii->setText(ii->text() + " - Object Pool Transfer");
                            ii->setBackgroundColor(QColor(Qt::green));
                        }
                        if( f.data[0] == 18)
                        {
                            ii->setText(ii->text() + " - End of Object Pool");
                            ii->setBackgroundColor(QColor(Qt::darkGreen));
                        }


                        if( f.data[0] == 173)
                        {
                            ii->setText(ii->text() + " - Change Active Mask");
                            ii->setBackgroundColor(QColor(255,192,203));
                        }

                        if( f.data[0] == 174)
                        {
                            ii->setText(ii->text() + " - Change Softkey Mask");
                            ii->setBackgroundColor(QColor(255,192,203));
                        }

                        if( f.data[0] == 34) ii->setText(ii->text() + " - Preferred Assignment");
                        if( f.data[0] == 35) ii->setText(ii->text() + " AUX-N Maintenance");

                        if( f.data[0] == 192) ii->setText(ii->text() + " - Get Memory");
                        if( f.data[0] == 194) ii->setText(ii->text() + " - Get Number of Soft Keys");
                        if( f.data[0] == 195) ii->setText(ii->text() + " - Get Text Font Data");
                        if( f.data[0] == 199) ii->setText(ii->text() + " - Get Hardware");
                        if( f.data[0] == 193) ii->setText(ii->text() + " - Get Supported WChars");
                        if( f.data[0] == 197) ii->setText(ii->text() + " - Get Window Mask Data");
                        if( f.data[0] == 198) ii->setText(ii->text() + " - Get Supported Objects");

                        if( f.data[0] == 223) ii->setText(ii->text() + " - Get Versions");
                        if( f.data[0] == 208) ii->setText(ii->text() + " - Store Version");
                        if( f.data[0] == 209) ii->setText(ii->text() + " - Load Version");
                        if( f.data[0] == 210) ii->setText(ii->text() + " - Delete Version");
                        if( f.data[0] == 211) ii->setText(ii->text() + " - Ext Get Versions");
                        if( f.data[0] == 212) ii->setText(ii->text() + " - Ext Store Version");
                        if( f.data[0] == 213) ii->setText(ii->text() + " - Ext Load Version");
                        if( f.data[0] == 214) ii->setText(ii->text() + " - Ext Delete Version");

                        if( f.data[0] == 160 ) ii->setText(ii->text() + " - Hide/Show Object");
                        if( f.data[0] == 161) ii->setText(ii->text() + " - Enable/Disable Object");
                        if( f.data[0] == 162) ii->setText(ii->text() + " - Select Input Object");
                        if( f.data[0] == 146) ii->setText(ii->text() + " - ESC");

                         if( f.data[0] == 165) ii->setText(ii->text() + " - Change Child Location");
                         if( f.data[0] == 180) ii->setText(ii->text() + " - Change Child Position");
                         if( f.data[0] == 168)
                         {
                             ii->setText(ii->text() + " - Change Numeric Value");
                         }
                         if( f.data[0] == 179)
                         {
                             ii->setText(ii->text() + " - Change String Value");
                         }


                         if( f.data[0] == 255)
                         {
                             ii->setText(ii->text() + " - Working Set Maintenance");
                             ii->setBackgroundColor(QColor(Qt::darkYellow));
                         }
                    }


                   // QTableWidgetItem *ii = new QTableWidgetItem();

                    if(f.pgn == 60416 )
                    {

                        ii->setText("TP.CM - ");
                        if(f.data[0] == 16) ii->setText(ii->text() + "RTS");
                        if(f.data[0] == 17) ii->setText(ii->text() + "CTS");
                        if(f.data[0] == 32) ii->setText(ii->text() + "BAM");
                        if(f.data[0] == 19) ii->setText(ii->text() + "End of Message Ack");
                        if(f.data[0] == 255) ii->setText(ii->text() + "Abort");

                        ii->setBackgroundColor(QColor(Qt::darkCyan));


                    }



                    if(f.pgn == 60160 )
                    {
                        ii->setText("TP.DT   (");

                            ii->setBackgroundColor(QColor(Qt::cyan));


                            for(int cc=1;cc<8;cc++)
                            {
                                ii->setText(ii->text()+QString("%1").arg(QChar(f.data[cc])));
                            }

                            ii->setText(ii->text()+")");
                    }

                    if( f.pgn == 51200)
                    {
                        ii->setText("ETP.CM - ");

                        if(f.data[0] == 20) ii->setText(ii->text() + "RTS");
                        if(f.data[0] == 21) ii->setText(ii->text() + "CTS");
                        if(f.data[0] == 22) ii->setText(ii->text() + "DPO");
                        if(f.data[0] == 23) ii->setText(ii->text() + "End of Message Ack");
                        if(f.data[0] == 255) ii->setText(ii->text() + "Abort");

                        for(int c=0; c< 8;c++)
                        {
                           ii->setBackgroundColor(QColor(Qt::darkCyan));

                        }
                    }



                    if( f.pgn == 50944)
                    {
                         ii->setBackgroundColor(QColor(Qt::cyan));
                        ii->setText("ETP.DT    (");
                        for(int c=1; c< 8;c++)
                        {


                               ii->setText(ii->text()+QString("%1").arg(QChar(f.data[c])));


                        }
                         ii->setText(ii->text()+")");
                    }


                    if( f.pgn == 60928)
                    {   // Address Claimed
                        bool selfcfg;
                        int ig;
                        int dci;
                        int dc;
                        int func;
                        int fi;
                        int ecui;
                        int man;
                        int id;

                        selfcfg = (f.data[7] & 0x80);
                        ig = (f.data[7] & 0x70) >> 4 ;
                        dci = f.data[7] & 0x0F;
                        dc = (f.data[6] & 0xFE) >> 1;
                        func = f.data[5];
                        fi = (f.data[4] & 0xF8) >> 3;
                        ecui = f.data[4] & 0x07;
                        man = (f.data[3] << 3) | ((f.data[2] & 0xE0)>>5);
                        id = ((f.data[2] & 0x1F) << 16) | (f.data[1]) << 8 | ( f.data[0]);
                        ii->setBackgroundColor(QColor(Qt::cyan));

                        ii->setText(QString("Man: %1 Grp: %2 Class: %3(%4) Func: %5(%6)[%7] Id: %8").arg(man).arg(ig).arg(dc).arg(dci).arg(func).arg(fi).arg(ecui).arg(id));

                        if(func == 29)
                        {
                            ii->setText(QString("VT Man: %1 Grp: %2 Class: %3(%4) Func: %5(%6)[%7] Id: %8").arg(man).arg(ig).arg(dc).arg(dci).arg(func).arg(fi).arg(ecui).arg(id));
                        }
                        if(func == 130 && ig == 2 && dc == 0)
                        {
                            ii->setText(QString("TC Man: %1 Grp: %2 Class: %3(%4) Func: %5(%6)[%7] Id: %8").arg(man).arg(ig).arg(dc).arg(dci).arg(func).arg(fi).arg(ecui).arg(id));
                        }


                    }



                    if( f.pgn == 59392)
                    {   // ACKNOWLEDGMENT
                        ii->setBackgroundColor(Qt::blue);
                        ii->setTextColor(Qt::white);
                        ii->setText("ACK: ");

                        if(f.data[0] == 0)
                        {
                            ii->setText(ii->text()+" OK");
                        }

                        if(f.data[0] == 1)
                        {
                            ii->setText(ii->text()+" Negative");
                        }

                        if(f.data[0] == 2)
                        {
                            ii->setText(ii->text()+" Access Denied");
                        }

                        if(f.data[0] == 3)
                        {
                            ii->setText(ii->text()+" Cannot Respond");
                        }
                    }
            //********************************************************************


        }





       // messageCount += d.count;








    }
    if(d.nMessages == 1) ui->pushButton->setText("OK");
}

void MainWindow::on_btSave_clicked()
{
    QString filename = QFileDialog::getSaveFileName(this,"Save","","ISOBUS Log (*.ibl)",0);

    QFile file(filename);

    if(file.open(QIODevice::WriteOnly))
    {
        QDataStream s(&file);

        ui->progressBar->setValue(0);
        ui->progressBar->setMaximum(frames.count());

        s << frames.count();
        for(int i=0;i<frames.count();i++)
        {
            s << frames[i].priority;
            s << frames[i].edp;
            s << frames[i].dp;
            s << frames[i].pf;
            s << frames[i].ps;
            s << frames[i].sa;
            s << frames[i].pgn;
            s << frames[i].ide;
            s << frames[i].dlc;
            for(int n=0;n<frames[i].dlc;n++)
            {
                s << frames[i].data[n];
            }

            ui->progressBar->setValue(i);
        }

        file.close();
        ui->progressBar->setValue(ui->progressBar->maximum());
    }
}

void MainWindow::on_btLoad_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,"Open","","ISOBUS Log (*.ibl)",0);
    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {

        QDataStream s(&file);

        int prog = 0;

        int count;
        s >> count;

        ui->progressBar->setValue(0);
        ui->progressBar->setMaximum(count);


        ui->tableWidget->clearContents();
        ui->tableWidget->setRowCount(0);
        frames.clear();


        for(int i = 0 ; i < count ; i++)
        {
            framex f;
            s >> f.priority;
            s >> f.edp;
            s >> f.dp;
            s >> f.pf;
            s >> f.ps;
            s >> f.sa;
            s >> f.pgn;
            s >> f.ide;
            s >> f.dlc;
            for(int n=0;n<f.dlc;n++)
            {
                s >> f.data[n];
            }

            for(int n=f.dlc; n<8;n++)
            {
                f.data[n] = 0x00;
            }


            dataPack d;
            d.nMessages = 0;

            d.count = 1;
            d.frames[0] = f;

            ISO(d);
            ui->progressBar->setValue(++prog);
            continue;

            frames.append(f);



            bool addRow = false;

            if(ui->cbFilter->isChecked())
            {


                for(int ii=0;ii<ui->filterTable->rowCount();ii++)
                {
                    QTableWidgetItem *n = ui->filterTable->item(ii,2);
                    if(n->checkState() == Qt::Checked)
                    {
                        if(f.sa == ui->filterTable->item(ii,0)->text().toInt() && f.ps == ui->filterTable->item(ii,1)->text().toInt()) addRow = true;


                    }
                }

                if(ui->cbAddressClaims->isChecked())
                {

                    if(f.pgn == 60928) addRow = true;
                    if(f.pgn == 59904)
                    {
                        int p = f.data[0] | (f.data[1] << 8) | (f.data[2] << 16);

                        if(p == 60928) addRow = true;
                    }
                }


            }

  /*          if(f.pgn == 61184 || (f.pgn >= 65280 && f.pgn <= 65535)) addRow = false;


                if (  ((f.sa == 38) && (f.ps == 129)) ||  ((f.sa == 129) && (f.ps == 38)) ) addRow = true;
                else addRow = false;

                if(f.sa == 129) addRow = true;


addRow = false;
            if(f.pgn == 59904) addRow = true;
            if(f.pgn == 60928) addRow = true;




            if(f.pgn == 58880 && f.sa == 38 && (f.ps == 129 || f.ps == 255)) addRow = true;
            if(f.pgn == 59136 && f.sa == 129 && (f.ps == 38 || f.ps == 255)) addRow = true;

            if(f.pgn == 60416 && f.sa == 38 && f.ps == 129) addRow=true;
            if(f.pgn == 60416 && f.sa == 129 && f.ps == 38) addRow=true;

            if(f.pgn == 60160 && f.sa == 38 && f.ps == 129) addRow=true;
            if(f.pgn == 60160 && f.sa == 129 && f.ps == 38) addRow=true;

            if( f.sa==129 && (f.pgn == 65037 || f.pgn == 65036) ) addRow = true;

           // addRow = false;
           // if( (f.pgn == 51456 )  && f.sa == 38) addRow = true;

            // Light Command
           // addRow = false;
           // if (f.pgn == 58880 && f.data[0] == 192) addRow = true;
           // if(f.pgn == 58880) addRow = true;

          //  if(f.sa == 28 && (f.pgn == 1165254 || f.pgn == 65256 || f.pgn==1165267) )addRow = true;
          //  if(f.pgn == 65097 && f.sa == 132) addRow = true;*/

            if(f.pgn == 65089) decodeLightCommand(f);
        if(f.pgn == 65091) decodePTO(f, ui->rearPTO);
        if(f.pgn == 65092) decodePTO(f, ui->frontPTO);

        if(f.pgn == 65041) decodeSCV(f, ui->scv1);
        if(f.pgn == 65042) decodeSCV(f, ui->scv2);
        if(f.pgn == 65043) decodeSCV(f, ui->scv3);
        if(f.pgn == 65044) decodeSCV(f, ui->scv4);
        if(f.pgn == 65054) decodeSCV(f, ui->scv14);

       // if(f.pgn == 65057) decodeSCVmeasured(f, ui->scvMeasured);
       // if(f.pgn == 65058) decodeSCVmeasured(f, ui->scvMeasured_2);



            //if(f.pgn != 55552) addRow = false;

           // addRow = false;
           // if ( ((f.sa == 240) && (f.ps == 255)) && f.pgn == 58880) addRow = true;

            if(addRow)
            {
            ui->tableWidget->setRowCount(ui->tableWidget->rowCount()+1);
            QTableWidgetItem *ii = new QTableWidgetItem();

            // Messages in RB

            // PRIORITY
            ii = new QTableWidgetItem(QString("%1").arg(f.priority));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,2,ii);
            // EDP
            ii = new QTableWidgetItem(QString("%1").arg(f.edp));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,3,ii);
            // DP
            ii = new QTableWidgetItem(QString("%1").arg(f.dp));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,4,ii);
            // EXID
            ii = new QTableWidgetItem(QString("%1").arg(f.ide));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,5,ii);
            // PF
            ii = new QTableWidgetItem(QString("%1").arg(f.pf));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,6,ii);
            // PS
            ii = new QTableWidgetItem(QString("%1").arg(f.ps));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,7,ii);
            // SA
            ii = new QTableWidgetItem(QString("%1").arg(f.sa));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,8,ii);
            // PGN
            ii = new QTableWidgetItem(QString("%1").arg(f.pgn));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,9,ii);
            // DLC
            ii = new QTableWidgetItem(QString("%1").arg(f.dlc));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,10,ii);
            // DATA
            for(int n=0;n<8;n++)
            {
                ii = new QTableWidgetItem(QString("%1").arg(f.data[n]));
                ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,11+n,ii);

                if( f.dlc < n+1)
                {
                    ii->setBackgroundColor(QColor(Qt::lightGray));
                    ii->setTextColor(QColor(Qt::gray));
                }
            }


            ii = new QTableWidgetItem(QString(""));
            ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,19,ii);

            if((f.pgn == 58880)  )
            {
                ii->setText("VT to ECU");
                if( f.data[0] == 0) ii->setText(ii->text() + " Soft Key Activation");
                if( f.data[0] == 1) ii->setText(ii->text() + " Button Activation");
                if( f.data[0] == 2) ii->setText(ii->text() + " Pointing Event");

                if( f.data[0] == 36) ii->setText(ii->text() + " AUX-N Assignment");

                if( f.data[0] == 37) ii->setText(ii->text() + " AUX-N Status Enable");
                if( f.data[0] == 38) ii->setText(ii->text() + " AUX-N Status");
                if( f.data[0] == 35) ii->setText(ii->text() + " AUX-N Maintenance");



                if( f.data[0] == 192) ii->setText(ii->text() + " - Get Memory Response");
                if( f.data[0] == 194) ii->setText(ii->text() + " - Get Number of Soft Keys Response");
                if( f.data[0] == 195) ii->setText(ii->text() + " - Get Text Font Data Response");
                if( f.data[0] == 199) ii->setText(ii->text() + " - Get Hardware Response");
                if( f.data[0] == 193) ii->setText(ii->text() + " - Get Supported WChars Response");
                if( f.data[0] == 197) ii->setText(ii->text() + " - Get Window Mask Data Response");
                if( f.data[0] == 198) ii->setText(ii->text() + " - Get Supported Objects Response");

                if( f.data[0] == 223) ii->setText(ii->text() + " - Get Versions Response");
                if( f.data[0] == 208) ii->setText(ii->text() + " - Store Version Response");
                if( f.data[0] == 209) ii->setText(ii->text() + " - Load Version Response");
                if( f.data[0] == 210) ii->setText(ii->text() + " - Delete Version Response");
                if( f.data[0] == 211) ii->setText(ii->text() + " - Ext Get Versions Response");
                if( f.data[0] == 212) ii->setText(ii->text() + " - Ext Store Version Response");
                if( f.data[0] == 213) ii->setText(ii->text() + " - Ext Load Version Response");
                if( f.data[0] == 214) ii->setText(ii->text() + " - Ext Delete Version Response");

                if( f.data[0] == 160 ) ii->setText(ii->text() + " - Hide/Show Object Response");
                if( f.data[0] == 161) ii->setText(ii->text() + " - Enable/Disable Object Response");
                if( f.data[0] == 162) ii->setText(ii->text() + " - Select Input Object Response");
                if( f.data[0] == 146) ii->setText(ii->text() + " - ESC Response");

                 if( f.data[0] == 165) ii->setText(ii->text() + " - Change Child Location Response");
                 if( f.data[0] == 180) ii->setText(ii->text() + " - Change Child Position Response");
                 if( f.data[0] == 168)
                 {
                     ii->setText(ii->text() + " - Change Numeric Value Response");
                 }
                 if( f.data[0] == 179)
                 {
                     ii->setText(ii->text() + " - Change String Value Response");
                 }




                if( f.data[0] == 254)
                {
                    ii->setText(ii->text() + " - VT Status");
                    ii->setBackgroundColor(QColor(Qt::yellow));
                }

                if( f.data[0] == 5)
                {
                    int val = 0;
                    for(int ic=7;ic>3;ic--)
                    {
                        val = val << 8;
                        val |= f.data[ic];
                    }
                    ii->setText(ii->text() + QString(" - VT Change Numeric Value (%1)").arg(val));
                }

                if( f.data[0] == 6)
                {
                    ii->setText(ii->text() + " - Change Active Mask");
                }

                if( f.data[0] == 7)
                {
                    ii->setText(ii->text() + " - Change Softkey Mask");
                }
            }

            if( (f.pgn == 59136)  )
            {
                ii->setText("ECU to VT");

                if( f.data[0] == 17)
                {
                    ii->setText(ii->text() + " - Object Pool Transfer");
                    ii->setBackgroundColor(QColor(Qt::green));
                }
                if( f.data[0] == 18)
                {
                    ii->setText(ii->text() + " - End of Object Pool");
                    ii->setBackgroundColor(QColor(Qt::darkGreen));
                }


                if( f.data[0] == 173)
                {
                    ii->setText(ii->text() + " - Change Active Mask");
                    ii->setBackgroundColor(QColor(255,192,203));
                }

                if( f.data[0] == 174)
                {
                    ii->setText(ii->text() + " - Change Softkey Mask");
                    ii->setBackgroundColor(QColor(255,192,203));
                }

                if( f.data[0] == 34) ii->setText(ii->text() + " - Preferred Assignment");

                if( f.data[0] == 192) ii->setText(ii->text() + " - Get Memory");
                if( f.data[0] == 194) ii->setText(ii->text() + " - Get Number of Soft Keys");
                if( f.data[0] == 195) ii->setText(ii->text() + " - Get Text Font Data");
                if( f.data[0] == 199) ii->setText(ii->text() + " - Get Hardware");
                if( f.data[0] == 193) ii->setText(ii->text() + " - Get Supported WChars");
                if( f.data[0] == 197) ii->setText(ii->text() + " - Get Window Mask Data");
                if( f.data[0] == 198) ii->setText(ii->text() + " - Get Supported Objects");

                if( f.data[0] == 223) ii->setText(ii->text() + " - Get Versions");
                if( f.data[0] == 208) ii->setText(ii->text() + " - Store Version");
                if( f.data[0] == 209) ii->setText(ii->text() + " - Load Version");
                if( f.data[0] == 210) ii->setText(ii->text() + " - Delete Version");
                if( f.data[0] == 211) ii->setText(ii->text() + " - Ext Get Versions");
                if( f.data[0] == 212) ii->setText(ii->text() + " - Ext Store Version");
                if( f.data[0] == 213) ii->setText(ii->text() + " - Ext Load Version");
                if( f.data[0] == 214) ii->setText(ii->text() + " - Ext Delete Version");

                if( f.data[0] == 160 ) ii->setText(ii->text() + " - Hide/Show Object");
                if( f.data[0] == 161) ii->setText(ii->text() + " - Enable/Disable Object");
                if( f.data[0] == 162) ii->setText(ii->text() + " - Select Input Object");
                if( f.data[0] == 146) ii->setText(ii->text() + " - ESC");

                 if( f.data[0] == 165) ii->setText(ii->text() + " - Change Child Location");
                 if( f.data[0] == 180) ii->setText(ii->text() + " - Change Child Position");
                 if( f.data[0] == 168)
                 {
                     int val;
                     val = f.data[4] | (f.data[5] << 8) | (f.data[6] << 16) | (f.data[7] << 24);
                     ii->setText(ii->text() + " - Change Numeric Value");
                     ii->setText(ii->text() + QString(" %1").arg(val));

                     if(val == 40038) ii->setBackgroundColor(Qt::green);
                     if(val == 40040) ii->setBackgroundColor(Qt::green);

                 }
                 if( f.data[0] == 179)
                 {
                     ii->setText(ii->text() + " - Change String Value");
                 }


                 if( f.data[0] == 255)
                 {
                     ii->setText(ii->text() + " - Working Set Maintenance");
                     ii->setBackgroundColor(QColor(Qt::darkYellow));
                 }
            }


            if(f.pgn == 60416 )
            {

                ii->setText("TP.CM - ");
                if(f.data[0] == 16) ii->setText(ii->text() + "RTS");
                if(f.data[0] == 17) ii->setText(ii->text() + "CTS");
                if(f.data[0] == 32) ii->setText(ii->text() + "BAM");
                if(f.data[0] == 19) ii->setText(ii->text() + "End of Message Ack");
                if(f.data[0] == 255) ii->setText(ii->text() + "Abort");

                ii->setBackgroundColor(QColor(Qt::darkCyan));


            }



            if(f.pgn == 60160 )
            {
                ii->setText("TP.DT   (");

                    ii->setBackgroundColor(QColor(Qt::cyan));


                    for(int cc=1;cc<8;cc++)
                    {
                        ii->setText(ii->text()+QString("%1").arg(QChar(f.data[cc])));
                    }

                    ii->setText(ii->text()+")");
            }

            if( f.pgn == 51200)
            {
                ii->setText("ETP.CM - ");

                if(f.data[0] == 20) ii->setText(ii->text() + "RTS");
                if(f.data[0] == 21) ii->setText(ii->text() + "CTS");
                if(f.data[0] == 22) ii->setText(ii->text() + "DPO");
                if(f.data[0] == 23) ii->setText(ii->text() + "End of Message Ack");
                if(f.data[0] == 255) ii->setText(ii->text() + "Abort");

                for(int c=0; c< 8;c++)
                {
                   ii->setBackgroundColor(QColor(Qt::darkCyan));

                }
            }



            if( f.pgn == 50944)
            {
                 ii->setBackgroundColor(QColor(Qt::cyan));
                ii->setText("ETP.DT    (");
                for(int c=1; c< 8;c++)
                {


                       ii->setText(ii->text()+QString("%1").arg(QChar(f.data[c])));


                }
                 ii->setText(ii->text()+")");
            }


            if( f.pgn == 60928)
            {   // Address Claimed
                bool selfcfg;
                int ig;
                int dci;
                int dc;
                int func;
                int fi;
                int ecui;
                int man;
                int id;

                selfcfg = (f.data[7] & 0x80);
                ig = (f.data[7] & 0x70) >> 4 ;
                dci = f.data[7] & 0x0F;
                dc = (f.data[6] & 0xFE) >> 1;
                func = f.data[5];
                fi = (f.data[4] & 0xF8) >> 3;
                ecui = f.data[4] & 0x07;
                man = (f.data[3] << 3) | ((f.data[2] & 0xE0)>>5);
                id = ((f.data[2] & 0x1F) << 16) | (f.data[1]) << 8 | ( f.data[0]);
                ii->setBackgroundColor(QColor(Qt::cyan));

                ii->setText(QString("Man: %1 Grp: %2 Class: %3(%4) Func: %5(%6)[%7] Id: %8").arg(man).arg(ig).arg(dc).arg(dci).arg(func).arg(fi).arg(ecui).arg(id));

                if(func == 29)
                {
                    ii->setText(QString("VT Man: %1 Grp: %2 Class: %3(%4) Func: %5(%6)[%7] Id: %8").arg(man).arg(ig).arg(dc).arg(dci).arg(func).arg(fi).arg(ecui).arg(id));
                }
                if(func == 130 && ig == 2 && dc == 0)
                {
                    ii->setText(QString("TC Man: %1 Grp: %2 Class: %3(%4) Func: %5(%6)[%7] Id: %8").arg(man).arg(ig).arg(dc).arg(dci).arg(func).arg(fi).arg(ecui).arg(id));
                }


            }

            if( f.pgn == 59392)
            {   // ACKNOWLEDGMENT
                ii->setBackgroundColor(Qt::blue);
                ii->setTextColor(Qt::white);
                ii->setText("ACK: ");

                if(f.data[0] == 0)
                {
                    ii->setText(ii->text()+" OK");
                }

                if(f.data[0] == 1)
                {
                    ii->setText(ii->text()+" Negative");
                }

                if(f.data[0] == 2)
                {
                    ii->setText(ii->text()+" Access Denied");
                }

                if(f.data[0] == 3)
                {
                    ii->setText(ii->text()+" Cannot Respond");
                }
            }

            }


           ui->progressBar->setValue(++prog);
        }



        file.close();
        ui->progressBar->setValue(ui->progressBar->maximum());
}
}


void MainWindow::on_btClear_clicked()
{
    int prog=0;
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum(ui->tableWidget->rowCount());


    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);

    frames.clear();
}


void MainWindow::on_tableWidget_itemSelectionChanged()
{

    QList<QTableWidgetSelectionRange> list = ui->tableWidget->selectedRanges();

    if(list.empty()) return;

    int width =  (list[0].rightColumn() - list[0].leftColumn())+1;

    int v;
    quint64 value = 0;
    int row = list[0].topRow();
    for(int i=0; i<width ; i++)
    {
        int col = list[0].rightColumn()-i;

        if(ui->tableWidget->item(row,col)!=0)v = ui->tableWidget->item(row,col)->text().toInt();
        value = value << 8;
        value |= (quint64)(unsigned int)v;
    }
    setWindowTitle(QString("%1  :  0x%2 : %3 : ").arg(value,0,10).arg(value,0,16).arg(value,0,2).toUpper());



}

void MainWindow::DataReady(int man, int dev)
{
    ui->textBrowser->append(QString("#%1 Manufacturer: 0x%2  Device: 0x%3").arg(ctr).arg(man,0,16).arg(dev,0,16));

    ctr++;
    setWindowTitle(QString("#%1").arg(ctr));
}

void MainWindow::on_pushButton_3_clicked()
{
    usb->writeCommand(0x03, 0x00);
}

void MainWindow::on_pushButton_4_clicked()
{
    usb2->writeFrame();
}

void MainWindow::on_rb_monitor_toggled(bool checked)
{

    if(checked)
    {
        if(usb->productID == 0x4d8 && usb->productID == 0x4b0) return;  // Already correct

      //  usb->terminate();
     //   usb->wait(10000);

     //   qDebug() << "X";

        disconnect(usb2,SIGNAL(ISO(dataPack)),this,SLOT(ISO(dataPack)));
        disconnect(usb2,SIGNAL(dataReady(int,int)), this, SLOT(DataReady(int,int)));

        disconnect(usb2,SIGNAL(attached()),this,SLOT(usb_attached()));
        disconnect(usb2,SIGNAL(removed()),this, SLOT(usb_removed()));

      //   usb->deleteLater();
     //   usb = new CUSBDevice(0x4d8,0x04b0,0x01);     // Monitor & Flash Control
      //usb = new CUSBDevice(0x4d8,0x04b1,0x01);    // Bridge (only way to monitor when bridge is in loopback mode)

         usb->checkAttached();
         connect(usb,SIGNAL(ISO(dataPack)),this,SLOT(ISO(dataPack)));
         connect(usb,SIGNAL(dataReady(int,int)), this, SLOT(DataReady(int,int)));

         connect(usb,SIGNAL(attached()),this,SLOT(usb_attached()));
         connect(usb,SIGNAL(removed()),this, SLOT(usb_removed()));
         if(usb->isPresent()) usb_attached();
         else ui->statusBar->showMessage("ISOBUS Board Not Attached");

       // usb->start();
    }

}

void MainWindow::on_rb_bridge_toggled(bool checked)
{
    if(checked)
    {
        if(usb->productID == 0x4d8 && usb->productID == 0x4b1) return;  // Already correct



      //  usb->terminate();
       // usb->wait(10000);

     //   qDebug() << "X";

        disconnect(usb,SIGNAL(ISO(dataPack)),this,SLOT(ISO(dataPack)));
        disconnect(usb,SIGNAL(dataReady(int,int)), this, SLOT(DataReady(int,int)));

        disconnect(usb,SIGNAL(attached()),this,SLOT(usb_attached()));
        disconnect(usb,SIGNAL(removed()),this, SLOT(usb_removed()));

        //delete usb;
      //  usb->deleteLater();
       // usb = new CUSBDevice(0x4d8,0x04b0,0x01);     // Monitor & Flash Control
      //usb = new CUSBDevice(0x4d8,0x04b1,0x01);    // Bridge (only way to monitor when bridge is in loopback mode)

        // usb->checkAttached();

         connect(usb2,SIGNAL(ISO(dataPack)),this,SLOT(ISO(dataPack)));
         connect(usb2,SIGNAL(dataReady(int,int)), this, SLOT(DataReady(int,int)));

         connect(usb2,SIGNAL(attached()),this,SLOT(usb_attached()));
         connect(usb2,SIGNAL(removed()),this, SLOT(usb_removed()));

         if(usb2->isPresent()) usb_attached();
         else ui->statusBar->showMessage("ISOBUS Board Not Attached");

        //usb->start();
    }
}

void MainWindow::scrollTimeout()
{
    ui->tableWidget->scrollToBottom();
}

void MainWindow::scrollBarPressed()
{
    scrollTimer.stop();
    ui->cbScroll->setChecked(false);
}

void MainWindow::scrollBarReleased()
{
  //  if(ui->tableWidget->verticalScrollBar()->value() == ui->tableWidget->verticalScrollBar()->maximum()) scrollTimer.start(1);
}

void MainWindow::on_cbScroll_clicked()
{
    if(ui->cbScroll->isChecked()) scrollTimer.start(1);
    else scrollTimer.stop();
}

void MainWindow::on_pushButton_5_clicked()
{
    refilter();
}
