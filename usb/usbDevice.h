#ifndef USB_DEVICE_H
#define USB_DEVICE_H

#include <QThread>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QInputDialog>
#include <QList>

#ifdef _WIN32
    #include "windows.h"
    #include "Dbt.h"
#endif

#include "math.h"
#include <libusb-1.0/libusb.h>
#include <QDebug>

class framex
{
public:
    int priority;
    bool edp;
    bool dp;
    bool ide;
    int pf;
    int ps;
    int sa;
    int dlc;
    int data[8];
    int pgn;
};

class dataPack
{
public:
    int nBytes;
    int nMessages;

    int id[4];
    int bytes[4][20];



    int count;

    framex frames[4];
};

Q_DECLARE_METATYPE(dataPack);
Q_DECLARE_METATYPE(framex);

class CUSBDevice : public QThread
{
	Q_OBJECT
public:
    CUSBDevice(unsigned int vendor, unsigned int product, unsigned int interface);
	~CUSBDevice();
	bool	isPresent();
    void    writeData(dataPack);
    void    writeCommand(char cmd, char param);

    void    writeFrame();

    unsigned int vendorID;
    unsigned int productID;
    unsigned int interfaceNumber;
protected:
	void run();
private:


	bool	AttachedState;
	bool	AttachedButBroken;
    QList<int> data;
    libusb_device_handle *handle;

    static int LIBUSB_CALL hotplug_callback(libusb_context *ctx, libusb_device *dev, libusb_hotplug_event event, void *user_data);

    char id;

    libusb_hotplug_callback_handle cb_handle;

public slots:
    bool	checkAttached();

signals:
    void readyRead(QString);
    void dataReady(int, int);
    void ISO(dataPack);

	void	info(QString);
	void	attached();
	void	removed();
};

#endif
