#include "usbDevice.h"

#include <QDebug>

CUSBDevice::CUSBDevice(unsigned int vendor, unsigned int product, unsigned int interface)
{
    setTerminationEnabled(true);
    id = 0;
    AttachedState = false;
    AttachedButBroken = false;

    vendorID = vendor;
    productID = product;
    interfaceNumber = interface;

    qRegisterMetaType<dataPack>("dataPack");

    handle = NULL;
    libusb_init(NULL);

    qDebug() << "USB CTOR";

    if(libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG))
    {
        int rc = libusb_hotplug_register_callback(NULL, (libusb_hotplug_event) (LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED | LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT), (libusb_hotplug_flag)(0), vendor, product, LIBUSB_HOTPLUG_MATCH_ANY, &hotplug_callback, this, &cb_handle);
        qDebug() << "HOTPLUG: " << QString((char*)libusb_error_name(rc));
    }
}

CUSBDevice::~CUSBDevice()
{
    if(handle != NULL) libusb_close(handle);
    libusb_exit(NULL);
}

bool CUSBDevice::checkAttached()
{
    bool found = false;
    libusb_device **list;

    int n = libusb_get_device_list(NULL, &list);
    libusb_device *dev;
    libusb_device_descriptor desc;

    for(int i=0;i<n;i++)
    {
        dev = list[i];
        libusb_get_device_descriptor(dev, &desc);

        if( desc.idVendor == vendorID && desc.idProduct == productID)
        {
            found = true;
            break;
        }

    }


    if(!AttachedState && found)
    {   // Found and need to open
        int r = libusb_open(dev, &handle);
        if(r==0)
        {   // Have it
            AttachedState = true;


            libusb_set_auto_detach_kernel_driver(handle, 1);
            libusb_claim_interface(handle, interfaceNumber);

            emit attached();
        }
        else
        {
            qDebug() << "USB Failed to Open";
        }
    }

    if(AttachedState && !found)
    {   // Device Removed
        AttachedState = false;
        libusb_close(handle);
        handle = NULL;
        emit removed();
    }


    libusb_free_device_list(list,1);
    return AttachedState;


}

bool CUSBDevice::isPresent()
{
    bool found = false;
    libusb_device **list;

    int n = libusb_get_device_list(NULL, &list);
    libusb_device *dev;
    libusb_device_descriptor desc;

    for(int i=0;i<n;i++)
    {
        dev = list[i];
        libusb_get_device_descriptor(dev, &desc);

        if( desc.idVendor == vendorID && desc.idProduct == productID)
        {
            found = true;
        }

    }

    libusb_free_device_list(list,1);
    return found;
}


void CUSBDevice::run()
{

    qDebug() << "USB Run";
    int bytesRead = 0;
    unsigned char buff[256];
    bool done = false;
    // Sit and read from device, emitting signal when data packet arrives
    while(!done)
    {
        if(AttachedState == true)
        {
            int suc = libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_IN | 2, buff, 64, &bytesRead, 10000);
            if( suc == LIBUSB_SUCCESS)
            {
              //  int man, dev;

              //  man = (unsigned char)buff[0];
  //              dev = (unsigned char)buff[1];

//                emit dataReady(man, dev);

               // qDebug() << "USB Message" << buff[0];

                dataPack d;

                int i;

                if(vendorID == 0x4d8 && productID == 0x4b0)
                {

                // Monitor Chip Mode
                d.nBytes = (unsigned char)buff[0];
                d.nMessages = (unsigned char)buff[1];

                d.count = (unsigned char) buff[54];

                i = 2;
                }

                if(vendorID == 0x4d8 && productID == 0x4b1)
                {

                // Monitor Chip Mode
                d.nBytes = (unsigned char)buff[0];
                d.nMessages = (unsigned char)buff[0];

                d.count = (unsigned char) buff[0];

                i = 1;
                }

//----------------------


                unsigned char t;

                for(int m=0;m<4;m++)
                {
                    // STD ID H (P3:P1 , EDP, DP, PF8:PF6

                    d.frames[m].priority = (unsigned char)buff[i];
                    d.frames[m].priority &= 0xE0;
                    d.frames[m].priority = d.frames[m].priority >> 5;

                    d.frames[m].pf = (unsigned char)buff[i];
                    d.frames[m].pf &= 0x07;
                    d.frames[m].pf = d.frames[m].pf << 5;

                    t = buff[i];

                    d.frames[m].edp = (t & 0x10);
                    d.frames[m].dp  = (t & 0x08);

                    i++;

                    // STD ID L ( PF5:PF3 , SRR, EXID, - , PF2:PF1

                    t = buff[i];

                    d.frames[m].ide = (t & 0x08);

                    t &= 0xE0;
                    t = t >> 3;
                    d.frames[m].pf |= (unsigned char)t;

                    t = buff[i];
                    t &= 0x03;
                    d.frames[m].pf |= (unsigned char)t;

                    i++;

                    // EXT ID H (PS8:PS1)

                    d.frames[m].ps = (unsigned char)buff[i++];

                    // EXT ID L (SA8:SA1)

                    d.frames[m].sa = (unsigned char)buff[i++];

                    // DLC ( - , RTR(1) , - , - , DLC3:DLC0 )

                    d.frames[m].dlc = (unsigned char)buff[i++];
                    d.frames[m].dlc &= 0x0F;

                    for(int n=0;n<8;n++)
                    {
                        d.frames[m].data[n] = (unsigned char)buff[i++];
                    }

                    int pgn = 0;

                    if(d.frames[m].edp) pgn |= 2;
                    if(d.frames[m].dp) pgn |=1;
                    pgn = pgn << 8;

                    pgn |= d.frames[m].pf;
                    pgn = pgn << 8;

                    if(d.frames[m].pf >= 240) pgn |= d.frames[m].ps;

                    d.frames[m].pgn = pgn;


                }
                emit ISO(d);
                //emit dataReady(V, C);
                // qDebug() << "USB Read";

                if(libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG))
                {
                    timeval tv;
                    tv.tv_sec = 0;
                    tv.tv_usec = 0;
                    libusb_handle_events_timeout_completed(NULL,&tv,NULL);
                }
            }
            else
            {
                sleep(1);
                emit info("Read Failed");
                emit info(QString((char*)libusb_error_name(suc)));
                qDebug() << "USB Read Failed: " << QString((char*)libusb_error_name(suc));


                if(libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG))
                {
                    timeval tv;
                    tv.tv_sec = 0;
                    tv.tv_usec = 0;
                    libusb_handle_events_timeout_completed(NULL,&tv,NULL);
                };
            }
        }
        else
        {

            if(libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG))
            {
                timeval tv;
                tv.tv_sec = 0;
                tv.tv_usec = 0;
                libusb_handle_events_timeout_completed(NULL,&tv,NULL);
            }
            sleep(1);
        }

    }
}


void CUSBDevice::writeData(dataPack v)
{
    int bytesWritten = 0;
    unsigned char data[80];
    data[0] = 0;
    data[1] = 0;

    /*if(v.section[0])data[0] = data[0] | 1;
    if(v.section[1])data[0] = data[0] | 2;
    if(v.section[2])data[0] = data[0] | 4;
    if(v.section[3])data[0] = data[0] | 8;
    if(v.section[4])data[0] = data[0] | 16;
    if(v.section[5])data[0] = data[0] | 32;*/


    if(AttachedState == true)
    {
        if( libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_OUT | 2, data, 64, &bytesWritten, 5000) != 0 )
        {
            emit info("Write Failed");
            //emit info(QString("%1 -- %2").arg(GetLastError()).arg((int)WriteHandleToUSBDevice));
        }

    }
}


void CUSBDevice::writeCommand(char cmd, char param)
{
    qDebug() << "Command: " << cmd << " - " << param;
    int bytesWritten = 0;
    unsigned char data[80];

    data[0] = cmd;
    data[1] = param;

    if(AttachedState == true)
    {
        if( libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_OUT | 2, data, 64, &bytesWritten, 5000) != 0 )
        {
            emit info("Write Failed");
            //emit info(QString("%1 -- %2").arg(GetLastError()).arg((int)WriteHandleToUSBDevice));
        }
        else
        {
            qDebug() << "OK";
        }

    }

}

void CUSBDevice::writeFrame()
{
    int bytesWritten = 0;
    unsigned char data[80];

    data[0] = 0x01 ;
    data[1] = 0xff;
    data[2] = 0x08;
    data[3] = 0xff;
    data[4] = 0xff;
    data[5] = 8;
    data[6] = 1;
    data[7] = 2;
    data[8] = 3;
    data[9] = 4;
    data[10] = 5;
    data[11] = 6;
    data[12] = 7;
    data[13] = id++;

    if(AttachedState == true)
    {
        if( libusb_interrupt_transfer(handle, LIBUSB_ENDPOINT_OUT | 2, data, 64, &bytesWritten, 5000) != 0 )
        {
            emit info("Write Failed");
            //emit info(QString("%1 -- %2").arg(GetLastError()).arg((int)WriteHandleToUSBDevice));
        }
        else
        {
            qDebug() << "OK";
        }

    }
}

 int  CUSBDevice::hotplug_callback(libusb_context *ctx, libusb_device *dev, libusb_hotplug_event event, void *user_data)
{
     (void)ctx;
     (void)dev;
     (void)event;
     (void)user_data;
     qDebug() << "Hotplug";
     static_cast<CUSBDevice*>(user_data)->checkAttached();
     return 0;
}

