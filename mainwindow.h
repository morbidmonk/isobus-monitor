#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <usb/usbDevice.h>

#include <QVector>
#include <QFile>
#include <QFileDialog>
#include <QVariant>
#include <QDataStream>
#include <QTableWidget>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
     CUSBDevice *usb;

     CUSBDevice *usb2;

public slots:
     void ISO(dataPack);
     void DataReady(int, int);

     void usb_attached();
     void usb_removed();


private slots:
    //void itemSelectionChanged();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_btSave_clicked();

    void on_btLoad_clicked();

    void on_btClear_clicked();

    void on_tableWidget_itemSelectionChanged();

    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();

    void on_rb_monitor_toggled(bool checked);

    void on_rb_bridge_toggled(bool checked);

    void scrollTimeout();
    void scrollBarPressed();
    void scrollBarReleased();
    void on_cbScroll_clicked();

    void refilter();

    void on_pushButton_5_clicked();

protected:
    bool nativeEvent(const QByteArray&, void*, long*);
private:
    QTimer scrollTimer;
    int messageCount;
    int twosCount;
    Ui::MainWindow *ui;

    QVector<framex> frames;

    int ctr;

    void decodeLightCommand(framex f);
    void setItemState(int index, int state);

    void decodePTO(framex f, QTableWidget* wid);
    void decodeSCV(framex f, QTableWidget* wid);
    void decodeSCVmeasured( framex f, QTableWidget* wid);

    void resizeEvent(QResizeEvent *e);


};

#endif // MAINWINDOW_H
