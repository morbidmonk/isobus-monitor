#-------------------------------------------------
#
# Project created by QtCreator 2019-09-27T22:56:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ISOBUS
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    usb/usbDevice.cpp \
    usb/utils.cpp

HEADERS  += mainwindow.h \
    usb/usbDevice.h \
    usb/utils.h

FORMS    += mainwindow.ui

LIBS +=  -lusb-1.0
